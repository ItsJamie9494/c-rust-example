# C and Rust Example

This is an example of combining C and Rust in the same GTK4 application.

The Rust code is stored in `rust-lib`, and exports a very simple FFI interface.

The C code is stored in `src`, and creates the Application.

The Rust code only creates a simple GtkButton, but theoretically could be used
to create the entire UI of an application, if desired. Although at that point,
just use Rust entirely.