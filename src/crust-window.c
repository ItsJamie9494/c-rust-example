/* crust-window.c
 *
 * Copyright 2022 Jamie Murphy
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "crust-window.h"
#include "libcrust.h"

struct _CrustWindow
{
  AdwApplicationWindow  parent_instance;

  /* Template widgets */
  GtkBox              *box;
};

G_DEFINE_FINAL_TYPE (CrustWindow, crust_window, ADW_TYPE_APPLICATION_WINDOW)

static void
crust_window_class_init (CrustWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/com/example/CandRust/crust-window.ui");
  gtk_widget_class_bind_template_child (widget_class, CrustWindow, box);
}

static void
crust_window_init (CrustWindow *self)
{
  CrustCustomButton *btn;

  gtk_widget_init_template (GTK_WIDGET (self));

  btn = crust_custom_button_new_with_label ("Custom Label from C");
  gtk_box_prepend (self->box, GTK_WIDGET (btn));
}
