#include <gtk/gtk.h>

#include "libcrust.h"

static void
test_custom_button_has_correct_type_info (void)
{
    GTypeQuery q;
    CrustCustomButton *custom_button;

    g_type_query (CRUST_TYPE_CUSTOM_BUTTON, &q);
    g_assert (q.type == CRUST_TYPE_CUSTOM_BUTTON);
    g_assert (q.type == crust_custom_button_get_type ());

    g_assert_cmpstr (q.type_name, ==, "CrustCustomButton");

    /* These test that the sizes of the structs in the header file actually match the
     * sizes of structs and the glib-subclass machinery in the Rust side.
     */
    g_assert (sizeof (CrustCustomButtonClass) == (gsize) q.class_size);
    g_assert (sizeof (CrustCustomButton) == (gsize) q.instance_size);

    custom_button = crust_custom_button_new();
    g_assert_nonnull (custom_button);
    g_assert (G_OBJECT_TYPE (custom_button) == CRUST_TYPE_CUSTOM_BUTTON);
    g_assert_finalize_object (custom_button);
}

static void
test_custom_button (void)
{
    CrustCustomButton *custom_button;

    custom_button = crust_custom_button_new ();
    g_assert_nonnull (custom_button);

    g_assert_cmpstr (gtk_button_get_label (GTK_BUTTON (custom_button)), ==, NULL);

    g_assert_finalize_object (custom_button);

    custom_button = crust_custom_button_new_with_label ("Test Label");
    g_assert_nonnull (custom_button);

    g_assert_cmpstr (gtk_button_get_label (GTK_BUTTON (custom_button)), ==, "Test Label");

    g_assert_finalize_object (custom_button);
}

int
main (int   argc,
      char *argv[])
{
    gtk_test_init (&argc, &argv, NULL);
    crust_init ();

    g_test_add_func ("/Crust/CustomButton/has_correct_type_info", test_custom_button_has_correct_type_info);
    g_test_add_func ("/Crust/CustomButton/new", test_custom_button);

    return g_test_run ();
}
