use gtk::glib as glib;

use glib::prelude::*;
use glib::translate::*;

use crate::widget::{CrustCustomButton, CustomButton};

use super::utils::FFI;

impl FFI<CustomButton> for CrustCustomButton {}

// TODO: Autogenerate
#[no_mangle]
pub unsafe extern "C" fn crust_custom_button_get_type() -> gtk::glib::ffi::GType {
    CustomButton::static_type().into_glib()
}

#[no_mangle]
pub unsafe extern "C" fn crust_custom_button_new() -> *const CrustCustomButton {
    CustomButton::default().to_glib_full()
}

#[no_mangle]
pub unsafe extern "C" fn crust_custom_button_new_with_label(label: *const libc::c_char) -> *const CrustCustomButton {
    let label: String = from_glib_none(label);

    CustomButton::with_label(&label).to_glib_full()
}
