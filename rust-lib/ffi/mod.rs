use std::sync::Once;

pub mod utils;
pub mod widget;

static IS_INITIALISED: Once = Once::new();

// TODO: Autogenerate this function
// This function takes all registered FFI types and initialises them
#[no_mangle]
pub unsafe extern "C" fn crust_init() {
    use self::widget::crust_custom_button_get_type;

    IS_INITIALISED.call_once(|| {
        gtk::init().expect("Gtk needs to be initialised");

        gtk::glib::gobject_ffi::g_type_ensure(crust_custom_button_get_type());
    });
}
