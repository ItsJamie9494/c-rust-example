use gtk::glib::{
    subclass::prelude::*,
    translate::{from_glib_none, FromGlibPtrNone},
    types::instance_of,
    StaticType,
};

/// Helper macro for converting Rust string literals to C char pointers
#[macro_export]
macro_rules! convert_to_c_str {
    ($txt:expr) => {
        // SAFETY: it's important that the type we pass to `from_bytes_with_nul` is 'static,
        // so that the storage behind the returned pointer doesn't get freed while it's still
        // being used. We get that by only allowing string literals.
        std::ffi::CStr::from_bytes_with_nul(concat!($txt, "\0").as_bytes())
            .unwrap()
            .as_ptr()
    };
}

/// Replacement for `g_return_if_fail()`.
#[macro_export]
macro_rules! return_if_fail {
    {
        $func_name:ident;
        $($condition:expr,)+
    } => {
        $(
            if !$condition {
                glib::ffi::g_return_if_fail_warning(
                    crate::convert_to_c_str!("Mozi"),
                    crate::convert_to_c_str!(stringify!($func_name)),
                    crate::convert_to_c_str!(stringify!($condition)),
                );
                return;
            }
        )+
    }
}

/// Replacement for `g_return_val_if_fail()`.
#[macro_export]
macro_rules! return_val_if_fail {
    {
        $func_name:ident => $retval:expr;
        $($condition:expr,)+
    } => {
        $(
            if !$condition {
                glib::ffi::g_return_if_fail_warning(
                    crate::convert_to_c_str!("Mozi"),
                    crate::convert_to_c_str!(stringify!($func_name)),
                    crate::convert_to_c_str!(stringify!($condition)),
                );
                return $retval;
            }
        )+
    }
}

/// Common functions and traits for an FFI object
pub trait FFI<R>
where
    Self: InstanceStruct,
    R: StaticType + FromGlibPtrNone<*const Self>,
{
    /// Safely check whether `Self` is a valid Rust object
    fn is_object(obj: *const Self) -> bool {
        unsafe { instance_of::<R>(obj as *const _) }
    }

    /// Safely get the Rust object from the C Instance
    fn object(obj: *const Self) -> R {
        unsafe { from_glib_none(obj) }
    }
}
