macro_rules! assert_initialized_main_thread {
    () => {
        if !::gtk::is_initialized_main_thread() {
            if ::gtk::is_initialized() {
                panic!("libcrust may only be used from the main thread.");
            } else {
                panic!("Gtk has to be initialized before using libcrust.");
            }
        }
    };
}

mod ffi;
pub mod widget;
