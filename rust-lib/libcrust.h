#ifndef CRUST_H
#define CRUST_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define CRUST_TYPE_CUSTOM_BUTTON             (crust_custom_button_get_type ())
#define CRUST_CUSTOM_BUTTON(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), CRUST_TYPE_CUSTOM_BUTTON, CrustCustomButton))
#define CRUST_CUSTOM_BUTTON_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), CRUST_TYPE_CUSTOM_BUTTON, CrustCustomButtonClass))
#define CRUST_IS_CUSTOM_BUTTON(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CRUST_TYPE_CUSTOM_BUTTON))
#define CRUST_IS_CUSTOM_BUTTON_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), CRUST_TYPE_CUSTOM_BUTTON))
#define CRUST_CUSTOM_BUTTON_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), CRUST_TYPE_CUSTOM_BUTTON, CrustCustomButtonClass))

GType crust_custom_button_get_type (void);

typedef struct _CrustCustomButton CrustCustomButton;
typedef struct _CrustCustomButtonClass CrustCustomButtonClass;

struct _CrustCustomButtonClass {
    GtkButtonClass parent;
};

struct _CrustCustomButton {
    GtkButton parent;
};

void crust_init (void);

CrustCustomButton *crust_custom_button_new (void);

CrustCustomButton *crust_custom_button_new_with_label (const char *label);

G_END_DECLS

#endif /* CRUST_H */
