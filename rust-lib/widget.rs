use glib::Object;
use gtk::glib;
use gtk::subclass::prelude::*;

#[repr(C)]
pub struct CrustCustomButtonClass {
    parent: gtk::ffi::GtkButtonClass,
}

unsafe impl ClassStruct for CrustCustomButtonClass {
    type Type = imp::CustomButton;
}

#[repr(C)]
pub struct CrustCustomButton {
    parent: gtk::ffi::GtkButton,
}

unsafe impl InstanceStruct for CrustCustomButton {
    type Type = imp::CustomButton;
}

mod imp {
    use super::*;

    #[derive(Default)]
    pub struct CustomButton;

    #[glib::object_subclass]
    impl ObjectSubclass for CustomButton {
        const NAME: &'static str = "CrustCustomButton";
        type Type = super::CustomButton;
        type ParentType = gtk::Button;

        type Instance = CrustCustomButton;
        type Class = CrustCustomButtonClass;
    }

    impl ObjectImpl for CustomButton {}
    impl WidgetImpl for CustomButton {}
    impl ButtonImpl for CustomButton {}
}

glib::wrapper! {
    pub struct CustomButton(ObjectSubclass<imp::CustomButton>)
        @extends gtk::Button, gtk::Widget,
        @implements gtk::Accessible, gtk::Actionable, gtk::Buildable, gtk::ConstraintTarget;
}

impl Default for CustomButton {
    fn default() -> Self {
        Self::new()
    }
}

impl CustomButton {
    pub fn new() -> Self {
        assert_initialized_main_thread!();
        Object::new()
    }

    pub fn with_label(label: &str) -> Self {
        assert_initialized_main_thread!();
        Object::builder().property("label", &label).build()
    }
}
